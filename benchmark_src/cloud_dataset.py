import numpy as np
import pandas as pd
import rasterio
import torch
from typing import Optional, List


class CloudDataset(torch.utils.data.Dataset):
    """Reads in images, transforms pixel values, & returns a dict containing
    chip_ids, img tensors, & label masks (where avail)
    """
    
    def __init__(self, x_paths: pd.DataFrame, bands: List[str],
                 y_paths: Optional[pd.DataFrame] = None,
                 transforms: Optional[list] = None,):
        """
        Instantiate CloudDataset class.
        
        Args:
            x_paths(pd.DataFrame): dataframe with row for each chip.  Requires
                a column for chip_id, and a column with the path to the TIF
                for each of the bands.
            bands(list[str]): list of the bands included in the data
            y_paths(pd.DataFrame, optional): a dataframe with chip & columns
                for chip_id
            transforms(list, optional): list of transforms to apply to the feature
                data (augmentations, etc)
        """
        self.data = x_paths
        self.label = y_paths
        self.transforms = transforms
        self.bands = bands
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, idx: int):
        # Load an n-channel image from a chip-level dataframe
        img = self.data.loc[idx]
        band_arrs = []
        for band in self.bands:
            with rasterio.open(img[f"{band}_path"]) as b:
                band_arr = b.read(1).astype("float32")
            band_arrs.append(band_arr)
        x_arr = np.stack(band_arrs, axis=-1)
        
        # Apply transforms if provided
        if self.transforms:
            x_arr = self.transforms(image=x_arr)["image"]
        x_arr = np.transpose(x_arr, [2, 0, 1])
        
        # Prepare dict for each item
        item = {"chip_id": img.chip_id, "chip": x_arr}
        
        # Load label if avail
        if self.label is not None:
            label_path = self.label.loc[idx].label_path
            with rasterio.open(label_path) as lp:
                y_arr = lp.read(1).astype("float32")
            # Apply same transforms to label
            if self.transforms:
                y_arr = self.transforms(image=y_arr)["image"]
            item["label"] = y_arr
            
        return item
