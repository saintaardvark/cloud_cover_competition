#!/usr/bin/env python3

# https://amt.copernicus.org/articles/13/1953/2020/
# https://amaarora.github.io/2020/09/13/unet.html

from loguru import logger

import torch.nn as nn
import torch


# From amaarora
class UnetTutorialBlock(nn.Module):
    def __init__(self, in_ch, out_ch):
        super().__init__()
        self.conv1 = nn.Conv2d(in_ch, out_ch, 3)
        self.relu = nn.ReLU()
        self.conv2 = nn.Conv2d(out_ch, out_ch, 3)

    def forward(self, x):
        return self.relu(self.conv2(self.relu(self.conv1(x))))


# Let's see if I can do cloudseg
# ...huh, I think this is working...
class CloudsegBlock(nn.Module):
    def __init__(self, in_ch=1, out_ch=1):
        """
        Cloudseg init
        """
        super().__init__()
        self.conv1 = nn.Conv2d(in_ch, out_ch, kernel_size=3, stride=1)
        self.batchnorm1 = nn.BatchNorm2d(out_ch)  # FIXME: that number totes picked at random
        self.relu = nn.ReLU()

    def forward(self, x):
        return self.relu(self.batchnorm1(self.conv1(x)))


class CloudsegEncoder(nn.Module):
    def __init__(self, chs=(3, 64, 128, 256, 512, 1024)):
        """
        Cloudseg Encoder
        """
        super().__init__()
        # FIXME: the "2" in this list of args is based on assumption
        # that we have two features: cloud & not-cloud.  Not at all
        # sure that's correct.
        self.enc_blocks = nn.ModuleList([CloudsegBlock(chs[i], chs[i+1]) for i in range(len(chs) - 1)])
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        logger.debug(self.enc_blocks)

    def forward(self, x):
        features = []  # or ftrs
        for block in self.enc_blocks:
            logger.debug(block)
            x = block(x)
            logger.debug(x.shape)
            features.append(x)
            x = self.pool(x)
        return features


def main():
    """
    main entry point
    """
    x = torch.randn(1, 1, 572, 572)

    unet_enc_block = UnetTutorialBlock(1, 64)
    logger.debug(f"Shape of x: {x.shape}")
    logger.debug(f"Shape of transformed block: {unet_enc_block(x).shape}")

    x = torch.randn(1, 3, 572, 572)
    cloudseg_enc_block = CloudsegEncoder()
    logger.debug(f"Shape of Cloudseg transformed block: {cloudseg_enc_block(x).shape}")
    for i in cloudseg_enc_block(x):
        logger.debug(f"Shape of Cloudseg transformed block: {i.shape}")


if __name__ == '__main__':
    main()
