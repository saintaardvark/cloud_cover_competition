from pathlib import Path
import random

import albumentations as A
import cv2
import click
from loguru import logger
import pandas as pd
import pytorch_lightning as pl
from pandas_path import path
import torch

from cloud_model import CloudModel
import cc_config

# To match benchmark
random.seed(9)

def get_path(chip_id, band):
    return cc_config.TRAIN_FEATURES / chip_id / f"{b}.tif"

@click.command()
@click.option("--short-run/--no-short-run",
              default=False,
              help="Short run for quick debugging")
@click.option("--add-transform/--no-add-transform",
              default=False,
              help="Short run for quick debugging")
def main(short_run, add_transform):
    """
    Main entry point
    """
    if not cc_config.TRAIN_FEATURES.exists():
        logger.warning("Uh oh, where's my data?")

    logger.debug("Reading metadata...")
    train_meta = pd.read_csv(cc_config.DATA_DIR / "train_metadata.csv")

    logger.debug("About to set bandpath...")
    for band in cc_config.BANDS:
        train_meta[f"{band}_path"] = cc_config.TRAIN_FEATURES / train_meta.chip_id / f"{band}.tif"

    logger.debug("About to set label path...")
    train_meta["label_path"] = cc_config.TRAIN_LABELS / (train_meta.chip_id + ".tif")

    # TODO: Alternate is to pass short_run as fast_dev_run arg to pl.Trainer up ahead.
    if short_run is True:
        logger.debug(f"Shape before dropping: {train_meta.shape}")
        logger.debug("Dropping most of the files to save time...")
        train_meta = train_meta.head()
        logger.debug(f"Shape after dropping: {train_meta.shape}")

    # Put 1/3 into validation set
    chip_ids = train_meta.chip_id.unique().tolist()
    val_chip_ids = random.sample(chip_ids, round(len(chip_ids) * 0.33))

    val_mask = train_meta.chip_id.isin(val_chip_ids)
    val = train_meta[val_mask].copy().reset_index(drop=True)
    train = train_meta[~val_mask].copy().reset_index(drop=True)

    logger.debug(f"Val shape: {val.shape}")
    logger.debug(f"Train shape: {train.shape}")

    # Separate features from labels
    feature_cols = ["chip_id"] + [f"{band}_path" for band in cc_config.BANDS]
    val_x = val[feature_cols].copy()
    val_y = val[["chip_id", "label_path"]].copy()

    train_x = train[feature_cols].copy()
    train_y = train[["chip_id", "label_path"]].copy()

    transform = None
    if add_transform is True:
        transform = A.Compose([
            A.RandomCrop(width=256, height=256),
            A.HorizontalFlip(p=0.5),
            A.RandomBrightnessContrast(p=0.2)
        ])

    # Set up pytorch_lightning.Trainer object
    # Training time seems to be about 50 mins (estimate, about 8 mins after starting) with batch size 16, num workers 8, and val_loader num_workers 0.
    # Oh, interesting: training time seems to be closer to 2h with val_loader num_worker set to 8.
    # ---scratch that; after 7 minutes, came down to ~ 48 mins...so about the same as above.
    cloud_model = CloudModel(
        bands=cc_config.BANDS,
        x_train=train_x,
        y_train=train_y,
        x_val=val_x,
        y_val=val_y,
        transform=transform,
        hparams={"batch_size": 16,   # 16 appears to work for the P4000 I'm on.
                 "num_workers": 8},  # GPU OOM
    )

    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        monitor="iou_epoch",
        mode="max",
        verbose=True,
    )

    early_stopping_callback = pl.callbacks.early_stopping.EarlyStopping(
        monitor="iou_epoch",
        patience=(cloud_model.patience * 3),
        mode="max",
        verbose=True,
    )

    device_count = torch.cuda.device_count()
    if device_count > 0:
        accelerator = None
        gpus = device_count
        auto_select_gpus = True
    else:
        accelerator = None
        gpus = None
        auto_select_gpus = False

    logger.debug(f"Accelerator: {accelerator}")
    logger.debug(f"GPUs: {gpus}")
    logger.debug(f"Auto select gpus: {auto_select_gpus}")

    trainer = pl.Trainer(
        accelerator=accelerator,
        gpus=gpus,
        auto_select_gpus=auto_select_gpus,
        fast_dev_run=False,
        callbacks=[checkpoint_callback, early_stopping_callback],
    )

    # And now we fit!

    # Note: tripped over https://github.com/pandas-dev/pandas/issues/42748 at this point:
    # AttributeError: 'functools.partial' object has no attribute '__name__'
    # https://stackoverflow.com/questions/68694047/pytorch-lightning-functools-partial-error
    #
    # reinstalled pandas 1.2.5.

    trainer.fit(model=cloud_model)
    logger.debug("About to save model using regular Pytorch ...")
    torch.save(cloud_model.state_dict(), cc_config.MODEL_SAVE_DIR / "cloud_model.pth")


if __name__ == '__main__':
    main()
