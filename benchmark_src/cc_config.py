from pathlib import Path

BANDS = ["B02", "B03", "B04", "B08"]

DATA_DIR = Path.cwd() / "../data"
TRAIN_FEATURES = DATA_DIR / "train_features"
TRAIN_LABELS = DATA_DIR / "train_labels"
MODEL_SAVE_DIR = Path.cwd() / "assets"
